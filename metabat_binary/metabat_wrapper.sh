#!/bin/bash
# Summary:
# --------
# This takes a depth.txt file and assembly.fa as input. It run metabat2 and
# formats the bin names and creates some dummy files so the
# MetagenomeUtils module won't complain.
#
# Assumptions:
# ------------
# It is assumed that the order of contigs in the depth.txt file is the same
# as in the assembly.fa
# 
usage()
{
cat<<EOF
  Usage: $0 [options] 
	<-a assembly fasta (required)> 
	<-d depth_file from jgi_summarize_bam_contig_depths (required)>
  <-m minimum contig size for metabat to consider [2500] (optional, don't go below 1kb))>
	<-o directory where the temp files will be written to, including the final bins. [Metabat_results] (optional)
	<-t threads [16] (optional)
  
	defaults are shown in square brackets

	example: $0 -a assembly.fa -t 32 -m 1500 -o Metabat_results -d MyAssembly.fa.depth.txt

	The output directory (i.e. Metabat_results) will contain the depth file (*.depth.txt)
	and the bins directory (Bins).  The naming of the bin fasta files have to be in the format
	<a string>.<3 digits>.fasta' (i.e. bin.234.fasta). The renaming of bins is done for you.

	
EOF
exit 1
}


while getopts 't:a:m:o:d:' OPTION
do 
  case $OPTION in  
  a)    ASSEMBLY="$OPTARG"
        ;;
  m)    MIN_CONTIG_LENGTH="$OPTARG"
        ;;
  o)    RESULTS="$OPTARG"
        ;;
  t)    THREADS="$OPTARG"
        ;;
  d)    DEPTH_FILE="$OPTARG"
        ;;
  ?)    echo usage
        exit 1
        ;;
  esac
done

# set defaults
MIN_CONTIG_LENGTH=${MIN_CONTIG_LENGTH:=2500}
RESULTS=${RESULTS:="Metabat_results"}
MAX_EDGES=${MAX_EDGES:=500}
THREADS=${THREADS:=16}
BINS='Bins'

if [ -z $ASSEMBLY ] && [ -z $DEPTH_FILE ]; then
	usage
fi


### ---------------------- ###
###        Functions       ###
testRun()
{
  command=$1
  errorCode=$2
  if [[ $errorCode != 0 ]]; then
    echo "Command failed: $command"
    echo "$errorCode"
    exit 1
  else
    echo "completed $command successfully"
  fi
}
### ---------------------- ###
set -x

# we should get full paths passed to us
# but lets make sure anyways so this script can be called
# independent of the narrative.kbase.us server.
ASSEMBLY=$(readlink -f $ASSEMBLY)
if [[ ! -s $ASSEMBLY ]]; then
  echo "$ASSEMBLY is missing or empty"
  exit 1
fi

# test that we have depth file
DEPTH_FILE=$(readlink -f $DEPTH_FILE)
if [[ ! -s $DEPTH_FILE ]]; then
  echo "$DEPTH_FILE is missing or empty"
  exit 1
fi

# test that we have required executables
which metabat2 > /dev/null 2>&1 
if [[ $? != 0 ]]; then
  echo "Failed to find metabat2 executable"
  exit 1
fi

# The python function that calls this metabat_wrapper.sh should have already
# created the RESULTS directory but just so this script can stand alone, I'll
# test for the directory and make it if it doesn't exist
if [[ -d $RESULTS ]]; then
  rm -rf $RESULTS  # remove old stuff
fi
mkdir -p $RESULTS 
if [[ ! -d $RESULTS ]]; then
  echo "Failed to create results directory: $RESULTS"
  exit 1
fi
cd $RESULTS
cp $DEPTH_FILE .  # depth file will be part of the output for users so copy it here

# run metabat
BINSDIR=${ASSEMBLY##*/}.metabat-bins
metabat2 --inFile $ASSEMBLY --outFile $BINSDIR/bin --abdFile $DEPTH_FILE --numThreads=$THREADS --minContig=$MIN_CONTIG_LENGTH --maxEdges=$MAX_EDGES --unbinned 
#testRun 'metabat2' $? 

# lets make the final output name standard, and not dependent on what
# the assembly was called. This way, downstream programs will be able to find it
# easier.
pwd
ls
if [[ ! -d $BINSDIR ]]; then
  echo "Failed to find the output bins directory: $BINSDIR folder. MetaBAT maybe failed."
  exit 1
fi
cp -r $BINSDIR $BINS
testRun "mv $BINSDIR $BINS" $? 

# The "binned_contigs_obj_ref" requires a file called *.summary so
# I'm creating a fake one.  The binned contigs object needs to be updated
# so this is not necessary.
echo -e "Bin name\tCompleteness\tGenome size\tGC content" > $BINS/notreal.summary
testRun "create $BINS/notreal.summary" $? 

# Unfortunately, the MetagenomeUtils function that loads the bins into "binned_contig_obj_ref" 
# uses a regular expression that requires the bin names to be in the form <something>.123.fasta
# So I'm converting bin names here.


# if we have bins, change their names so that MetagenomeUtils will
# recognize them.
# Test that metabat produced some bins
BINSARRAY=($(find $BINS -name "*.fa"))
testRun "find $BINS -name '*.fa'" $?
COUNT=0
for i in ${BINSARRAY[@]}; do
    if [[ $i =~ "unbinned" ]]; then
      newID="unbinned"
    else
      COUNT=$((COUNT+1))
      binID=$(basename $i) 
      binID=${binID%.fa}
      binID=${binID#bin.}
      newID=$(printf "%03d\n" $binID)
    fi
    
    mv $i $BINS/bin.$newID.fasta
    
    # lets populate the .summary file (this is a work around until MetagenomeUtils gets fixed)
    echo -e "bin.$newID.fasta\t50%\t10000\t50" >> $BINS/notreal.summary
done

if [[ $COUNT == 0 ]]; then
  echo Metabat did not produce any bins
  exit 0
fi

# cleanup large files
rm -r $BINSDIR

