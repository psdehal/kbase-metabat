#!/usr/bin/env python
import sys
import os
import time
import json
from Bio import SeqIO
from pprint import pprint

def log(message, prefix_newline=False):
    """Logging function, provides a hook to suppress or redirect log messages."""
    print(('\n' if prefix_newline else '') + '{0:.2f}'.format(time.time()) + ': ' + str(message))


class CombineDepthTables:
    MYASSEMBLY_DEPTH_FILE = 'merged.depth.txt'
    
    def createDictFromDepthfile(self, depth_file_path):
        # keep contig order (required by metabat2)
        depth_file_dict = {}
        with open(depth_file_path,'r') as f:
            header = f.readline().split()
            for line in f:
                vals = line.split()
                depth_file_dict[vals[0]] = vals[1:]

            depth_file_dict['header'] = header
        return depth_file_dict
    
    def createFinalDepthfile(self, scratch, local_depth_files, assembly):
        """ This function merges all the depth.txt files created by jgi_summarize_bam_contig_depths
            that was run in parallel. This function takes a list of dictionaries, the data_list param 
            (where each dictionary is the depth.txt file). "contig_list" contains the contig names in
            the same order as the assembly. This is necessary so the final merged depth.txt table has
            the same order as assembly.
        """ 
        header_list = []
        data_list = []
        main_dict = {}
        
        # make a list of contigs in the order that they
        # occur in the assembly. We need the final depth.txt file to
        # reflect this order.
        contig_list=[]

        if not os.path.exists(assembly):
            log("Assembly file not found in give path: {}".format(assembly))
            sys.exit(1)
        
        for seq_record in SeqIO.parse(assembly, "fasta"):
            contig_list.append(seq_record.id)
                    

        # create dictionary out of depth.txt files
        for f in local_depth_files:
            depth_file_dict = self.createDictFromDepthfile(os.path.abspath(f))
            data_list.append(depth_file_dict)

        #
        # an example header
        # header=['contigName','contigLen','totalAvgDepth','25.inter.fastq.sam.sorted.bam','25.inter.fastq.sam.sorted.bam-var']
        #

        #
        # grab the names of the first three columns 
        # since these will be consistent between tables ('contigName','contigLen','totalAvgDepth')
        #
        header_list = ['contigName','contigLen','totalAvgDepth']
        # create master dictionary of all contig names and their sizes
        for depth_dict in data_list:
            for contig in depth_dict:
                if contig == 'header':
                    continue
                main_dict[contig] = []
                main_dict[contig].append(depth_dict[contig][0])
                main_dict[contig].append(0)  # this is a space holder for totalAvgDepth

        #
        # merge each dictionary to main 
        #
        for depth_dict in data_list:
            # add to header, the bam and var header names
            header_names = depth_dict['header']
            header_list.append(depth_dict['header'][3])
            header_list.append(depth_dict['header'][4])

            # iterate through main_dict so in cases were the 
            # contig isn't found in the smaller dictionary, we can add '0' values
            for contig in main_dict:
                # ignore header key
                if contig == 'header':
                    continue

                # append bam depth and var values to main dictionary
                if contig in depth_dict:
                    # if no totalAvgDepth value has been added yet, then do so
                    try: 
                        old_coverage = main_dict[contig][1]
                    except IndexError:
                        old_coverage = 0
                        main_dict[contig].append(0)

                    # calculate totalAvgDepth
                    new_coverage = depth_dict[contig][2]
                    main_dict[contig][1] = float(old_coverage) + float(new_coverage)

                    # append bam coverage and variance
                    main_dict[contig].append(depth_dict[contig][2])
                    main_dict[contig].append(depth_dict[contig][3])
                else:
                    # add zero depth and variance
                    main_dict[contig].append(float(0))
                    main_dict[contig].append(float(0))

        # write merged table
        full_path_depth_file = os.path.join(scratch, self.MYASSEMBLY_DEPTH_FILE)
        
        with open(full_path_depth_file,'w') as f:
                f.write("\t".join(header_list) + '\n')
                for key in contig_list:
                    if key in main_dict:
                        f.write("{}\t{}\n".format(key,"\t".join([str(i) for i in main_dict[key]]) ) )
                        
        if os.path.exists(full_path_depth_file):
            print("FOUND  full_path_depth_file: {}".format(full_path_depth_file))
        else:
            print("NOTFOUND full_path_depth_file: {}".format(full_path_depth_file))
            
        return full_path_depth_file
