
import time
import json
import os
import uuid
import errno
import subprocess
import sys
import zipfile
import glob
import copy
import pandas
import re
import json

from pprint import pprint

from DataFileUtil.DataFileUtilClient import DataFileUtil
from KBaseReport.KBaseReportClient import KBaseReport
from ReadsUtils.ReadsUtilsClient import ReadsUtils
from AssemblyUtil.AssemblyUtilClient import AssemblyUtil
from MetagenomeUtils.MetagenomeUtilsClient import MetagenomeUtils
from KBParallel.KBParallelClient import KBParallel
from metabat.Utils.combineDepthTablesUtil import CombineDepthTables

def log(message, prefix_newline=False):
    """Logging function, provides a hook to suppress or redirect log messages."""
    print(('\n' if prefix_newline else '') + '{0:.2f}'.format(time.time()) + ': ' + str(message))


class MetabatUtil:
    METABAT_WORKING_DIR = 'Metabat_results'
    METABAT_BIN_DIR = 'Bins'
    MAX_NODES = 6
    BBMAP_THREADS=16
    BBMAP_MEM='30g'
    
    def validate_run_metabat_params(self, params):
        """
        _validate_run_metabat_params:
        validates params passed to run_metabat method
        """
        log('Start validating run_metabat params')

        # check for required parameters
        for p in ['assembly_ref', 'binned_contig_name', 'workspace_name', 'reads_list']:
            if p not in params:
                raise ValueError('"{}" parameter is required, but missing'.format(p))
    
    def _mkdir_p(self, path):
        """
        _mkdir_p: make directory for given path
        """
        if not path:
            return
        try:
            os.makedirs(path)
        except OSError as exc:
            if exc.errno == errno.EEXIST and os.path.isdir(path):
                pass
            else:
                raise
        
    def generate_alignment_bams(self, task_params):
        """
            This function runs the bbmap.sh alignments and creates the
            bam files from sam files using samtools.
        """
        # even though read_name is just one file, make read_name a
        # list because that is what stage_reads_list_file expects
        read_name = [task_params['fastq']]  
        
        # we need to copy the fastq file from workspace to local scratch.
        # 'fastq' should be path to one file (if we had read.fwd.fq and read.rev.fq then they
        # should have been interleaved already). However, I should interleave them in case
        # this function gets called other than the narrative.
        # this should return a list of 1 fastq file path on scratch
        read_scratch_path = self.stage_reads_list_file(read_name)
        # params['read_scratch_file'] = read_scratch_path
        
        #
        # grab the assembly. It should already exist on 'local' node but
        # needs to be copied over to scratch if on njsw node (assuming we're running
        # jobs in parallel)
        #
        if os.path.exists(task_params['contig_file_path']):
            assembly =  task_params['contig_file_path']
            print("FOUND ASSEMBLY ON LOCAL SCRATCH")
        else:
            # we are on njsw so lets copy it over to scratch
            assembly = self.get_contig_file(task_params['assembly_ref'])        
        
        min_contig_length = task_params['min_contig_length']
        
        fastq = read_scratch_path[0]
        result_directory = task_params['result_directory']
        sam = os.path.basename(fastq) + '.sam'
        sam = os.path.join(result_directory, sam)
    
        command = '/bin/bash bbmap.sh -Xmx{} fast threads={} ref={} in={} out={} mappedonly nodisk overwrite'.format(self.BBMAP_MEM,self.BBMAP_THREADS,assembly,fastq,sam)

        log('running alignment command: {}'.format(command))
        out,err = self._run_command(command)
        
        # create bam files from sam files
        command = 'samtools view -F 0x04 -uS ' + sam + ' | samtools sort - -o ' + sam + '.sorted.bam'
        
        log('running samtools command: {}'.format(command))
        self._run_command(command)
        
        bam_path = {}
        sorted_bam = sam + '.sorted.bam'
        
        # verify we got bams
        if not os.path.exists(sorted_bam):
            log('Failed to find bam file\n{}'.format(sorted_bam))
            sys.exit(1)
        elif(os.stat(sorted_bam).st_size == 0):
            log('Bam file is emtpy\n{}'.format(sorted_bam))
            sys.exit(1)
        
        #
        # create the depth file for this bam
        #
        assembly_basename=os.path.basename(assembly)

        depth_file_path  = os.path.join(self.scratch, str(uuid.uuid4()) + '.depth.txt')
        self._mkdir_p(assembly_basename + '.d')
        command = 'jgi_summarize_bam_contig_depths '
        command += '--outputDepth {} --minContigLength {} --minContigDepth 1 '.format(depth_file_path, min_contig_length)
        command += sorted_bam
        
        log('running alignment command: {}'.format(command))
        self._run_command(command)
        return depth_file_path
    
    def _generate_metabat_command(self, params):
        """
        _generate_command: generate metabat_wrapper.pl params
        """
        
        command = "/bin/bash "
        command += 'metabat_wrapper.sh'
        command += ' -a {} -m {} -o {} -d {} -t {}'.format(params.get('contig_file_path'),
                                         params.get('min_contig_length'),
                                         params.get('result_directory'),
                                         params.get('depth_file'),
                                         self.BBMAP_THREADS)

        log('Generated run_metabat command: {}'.format(command))

        return command
    
    def _run_command(self, command):
        """
        _run_command: run command and print result
        """
        os.chdir(self.scratch)
        log('Start executing command:\n{}'.format(command))
        log('Command is running from:\n{}'.format(self.scratch))
        pipe = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
        output,stderr = pipe.communicate()
        exitCode = pipe.returncode

        if (exitCode == 0):
            log('Executed command:\n{}\n'.format(command) +
                'Exit Code: {}\n'.format(exitCode))
        else:
            error_msg = 'Error running command:\n{}\n'.format(command)
            error_msg += 'Exit Code: {}\nOutput:\n{}\nStderr:\n{}'.format(exitCode, output, stderr)
            raise ValueError(error_msg)
            sys.exit(1)
        return (output,stderr)
    
    def stage_reads_list_file(self, reads_list):
        """
        _stage_reads_list_file: download fastq file associated to reads to scratch area
                          and return result_file_path
        """

        log('Processing reads object list: {}'.format(reads_list))
        
        result_file_path = []

        # getting from workspace and writing to scratch. The 'reads' dictionary now has file paths to scratch.
        reads = self.ru.download_reads({'read_libraries': reads_list, 'interleaved': None})['files']

        # reads_list is the list of file paths on workspace? (i.e. 12804/1/1).
        # "reads" is the hash of hashes where key is "12804/1/1" or in this case, read_obj and
        # "files" is the secondary key. The tertiary keys are "fwd" and "rev", as well as others.
        for read_obj in reads_list:
            files = reads[read_obj]['files']    # 'files' is dictionary where 'fwd' is key of file path on scratch.
            result_file_path.append(files['fwd'])
            if 'rev' in files and files['rev'] is not None:
                result_file_path.append(files['rev'])

        return result_file_path

    def get_contig_file(self, assembly_ref):
        """
        _get_contig_file: get contig file from GenomeAssembly object
        """
        # getting from workspace and writing to scratch.
        contig_file = self.au.get_assembly_as_fasta({'ref': assembly_ref}).get('path')
        
        sys.stdout.flush()
        contig_file = self.dfu.unpack_file({'file_path': contig_file})['file_path']

        return contig_file

    def _generate_output_file_list(self, result_directory):
        """
        _generate_output_file_list: zip result files and generate file_links for report
        """
        log('Start packing result files')
        output_files = list()

        output_directory = os.path.join(self.scratch, str(uuid.uuid4()))
        self._mkdir_p(output_directory)
        result_file = os.path.join(output_directory, 'metabat_result.zip')
        report_file = None

        # create a zip file object
        with zipfile.ZipFile(result_file, 'w',
                             zipfile.ZIP_DEFLATED,
                             allowZip64=True) as zip_file:
            
            # grab all files we want to zip
            for dirname, subdirs, files in os.walk(result_directory):
                if (dirname.endswith(self.METABAT_BIN_DIR)):
                    baseDir=os.path.basename(dirname)
                    for file in files:
                        if (file.endswith('.summary')):
                            continue
                        full=os.path.join(dirname, file)
                        zip_file.write(full,os.path.join(baseDir,file))
                for file in files:
                    if (file.endswith('.depth.txt')):
                        zip_file.write(os.path.join(dirname, file),file)


        output_files.append({'path': result_file,
                             'name': os.path.basename(result_file),
                             'label': os.path.basename(result_file),
                             'description': 'Files generated by MetaBAT2 App'})

        return output_files

    def _generate_html_report(self, result_directory, assembly_ref, binned_contig_obj_ref):
        """
        _generate_html_report: generate html summary report
        """

        log('Start generating html report')
        html_report = list()

        output_directory = os.path.join(self.scratch, str(uuid.uuid4()))
        self._mkdir_p(output_directory)
        result_file_path = os.path.join(output_directory, 'report.html')

        # get summary data from existing assembly object and bins_objects
        Summary_Table_Content = ''
        Overview_Content = ''
        (binned_contig_count, input_contig_count,
         total_bins_count) = self._generate_overview_info(assembly_ref,
                                                          binned_contig_obj_ref,
                                                          result_directory)

        Overview_Content += '<p>Binned contigs: {}</p>'.format(binned_contig_count)
        Overview_Content += '<p>Input contigs: {}</p>'.format(input_contig_count)
        Overview_Content += '<p>Number of bins: {}</p>'.format(total_bins_count)

        with open(result_file_path, 'w') as result_file:
            with open(os.path.join(os.path.dirname(__file__), 'report_template.html'),
                      'r') as report_template_file:
                report_template = report_template_file.read()
                report_template = report_template.replace('<p>Overview_Content</p>',
                                                          Overview_Content)
                report_template = report_template.replace('Summary_Table_Content',
                                                          Summary_Table_Content)
                result_file.write(report_template)

        html_report.append({'path': result_file_path,
                            'name': os.path.basename(result_file_path),
                            'label': os.path.basename(result_file_path),
                            'description': 'HTML summary report for MetaBAT2 App'})
        return html_report

    def _generate_overview_info(self, assembly_ref, binned_contig_obj_ref, result_directory):
        """
        _generate_overview_info: generate overview information from assembly and binnedcontig
        """
        
        # get assembly and binned_contig objects that already have some data populated in them
        assembly = self.dfu.get_objects({'object_refs': [assembly_ref]})['data'][0]
        binned_contig = self.dfu.get_objects({'object_refs': [binned_contig_obj_ref]})['data'][0]
        
        input_contig_count = assembly.get('data').get('num_contigs')
        bins_directory = os.path.join(self.scratch, self.METABAT_WORKING_DIR, self.METABAT_BIN_DIR)
        binned_contig_count = 0        
        total_bins_count = 0        
        total_bins = binned_contig.get('data').get('bins')
        total_bins_count = len(total_bins)
        for bin in total_bins:
            binned_contig_count += len(bin.get('contigs'))

        return (binned_contig_count, input_contig_count, total_bins_count)

    def _generate_report(self, binned_contig_obj_ref, params):
        """
        generate_report: generate summary report

        """
        log('Generating report')

        output_files = self._generate_output_file_list(params['result_directory'])

        output_html_files = self._generate_html_report(params['result_directory'],
                                                       params['assembly_ref'],
                                                       binned_contig_obj_ref)

        report_params = {
              'message': '',
              'workspace_name': params.get('workspace_name'),
              'file_links': output_files,
              'html_links': output_html_files,
              'direct_html_link_index': 0,
              'html_window_height': 266,
              'report_object_name': 'metabat_report_' + str(uuid.uuid4())}

        kbase_report_client = KBaseReport(self.callback_url)
        output = kbase_report_client.create_extended_report(report_params)

        report_output = {'report_name': output['name'], 'report_ref': output['ref']}

        return report_output
    
    def createDictFromDepthfile(self, depth_file_path):
        # keep contig order (required by metabat2)
        depth_file_dict = {}
        with open(depth_file_path,'r') as f:
            header = f.readline().rstrip().split("\t")
            # print('HEADER1 {}'.format(header))
            # map(str.strip, header)
            
            for line in f:
                # deal with cases were fastq name has spaces.Assume first
                # non white space word is unique and use this as ID.
                # line = line.rstrip()
                vals = line.rstrip().split("\t")
                if ' ' in vals[0]:
                    ID = vals[0].split()[0]
                else:
                    ID = vals[0]
                
                depth_file_dict[ID] = vals[1:]

            depth_file_dict['header'] = header

        return depth_file_dict
    
    def __init__(self, config):
        self.callback_url = config['SDK_CALLBACK_URL']
        self.scratch = config['scratch']
        self.shock_url = config['shock-url']
        self.dfu = DataFileUtil(self.callback_url)
        self.ru = ReadsUtils(self.callback_url)
        self.au = AssemblyUtil(self.callback_url)
        self.mgu = MetagenomeUtils(self.callback_url)
        self.parallel_runner = KBParallel(self.callback_url)
        
    def set_up_tasks(self, params):
        """
        This function runs bbmap.sh (from bbtools) to do the alignments. KBParallel
        is called and the tasks are run in parallel, up to 5 total nodes including local.
        """
        log('--->\nrunning MetabatUtil.set_up_tasks\n')# +
            # 'params:\n{}'.format(json.dumps(params, indent=1)))
        
        assembly = params['contig_file_path']
        reads_list = params['reads_list']
        min_contig_length = params['min_contig_length']

        # create an out directory for results
        # this should be the directory that has all the temp files (including bin dir)
        result_directory = os.path.join(self.scratch, self.METABAT_WORKING_DIR)
        self._mkdir_p(result_directory)
        params['result_directory'] = result_directory
            
        # lets create the tasks that will be run in parallel
        tasks = []        
        for fastq in reads_list:
            print("ADDING FASTQ FILE".format(fastq))
            task_params = copy.deepcopy(params)
            task_params['fastq'] = fastq
            tasks.append( {'module_name': 'metabat',
                            'function_name': 'run_alignments',
                            'version': 'dev',
                            'parameters': task_params
                          } )
       
        # calculate how many nodes we need (max=5)
        num_nsjw_nodes_required=0
        num_fastq = len(reads_list)
        if num_fastq < self.MAX_NODES:
            num_nsjw_nodes_required = num_fastq
        else:
            num_nsjw_nodes_required = self.MAX_NODES
            num_nsjw_nodes_required -= 1    

        # kbparallel requires these params
        batch_run_params = {'tasks': tasks,
                            'runner': 'parallel',
                            'concurrent_local_tasks': 1,
                            'concurrent_njsw_tasks': 0, #num_nsjw_nodes_required,
                            'max_retries': 2}
        
        # We'll run the alignments in parallel here
        log('--->\nlaunching parallel jobs\n')     
        kbparallel_results = self.parallel_runner.run_batch(batch_run_params)
        
        # print('KBPARALLLEL RESULTS')
        # pprint(kbparallel_results)
        # sys.exit()
        
        # ======================== #
        # back on the master node.        
        # write depth.txt files to json files before trying to
        # merge them into one file for debugging reasons
        # -------------------------------------------------
        # check for errors returned by njsw remote jobs
        for fd in kbparallel_results['results']:
            if fd['result_package']['error'] is not None:
                log('kb_parallel failed to complete without throwing an error on at least one of the kbparallel-njsw nodes.')
                sys.exit(1)
        
        depth_file_paths = []
        c=0
        for fd in kbparallel_results['results']:
            c += 1
            json_file = str(c) + ".depth.json"
            depth_dict = fd['result_package']['result'][0]
            outjson = os.path.join(self.scratch,json_file)
            with open(outjson, 'w') as fp:
                json.dump(depth_dict, fp)
            depth_file_paths.append(outjson)
                
        # finally, create merged depth.txt file
        self.combine = CombineDepthTables()
        final_depth_file = self.combine.createFinalDepthfile(self.scratch, depth_file_paths, assembly)
        return final_depth_file
    
    def run_metabat(self, params):
        """
        run_metabat: run_metabat.pl app

        required params:
            assembly_ref: Metagenome assembly object reference
            binned_contig_name: BinnedContig object name and output file header
            workspace_name: the name of the workspace it gets saved to.
            reads_list: list of reads object (PairedEndLibrary/SingleEndLibrary)
            upon which MetaBAT2 will be run

        optional params:
            min_contig_length: minimum contig length; default 1000

            ref: http://downloads.jbei.org/data/microbial_communities/MetaBAT/README.txt
        """
        log('--->\nrunning MetabatUtil.run_metabat\n' + 
            'params:\n{}'.format(json.dumps(params, indent=1)))
        
        #
        # Run metabat
        #
        result_directory = os.path.join(self.scratch, self.METABAT_WORKING_DIR)
        params['result_directory'] = result_directory
        
        command = self._generate_metabat_command(params)
        self._run_command(command)
        
        
        #########################
        # now for the report
        #########################
        # the file directory passed to file_directory should be a folder that
        # only contains the binned fasta files. This is what metagenomeUtils expects.
        generate_binned_contig_param = {
            'file_directory': os.path.join(result_directory,self.METABAT_BIN_DIR),
            'assembly_ref': params['assembly_ref'],
            'binned_contig_name': params['binned_contig_name'],
            'workspace_name': params['workspace_name']
        }
        
        # save & upload contigs & returns ref
        binned_contig_obj_ref = self.mgu.file_to_binned_contigs(generate_binned_contig_param).get('binned_contig_obj_ref')
        
        reportVal = self._generate_report(binned_contig_obj_ref, params)
        
        returnVal = {
            'result_directory': result_directory,
            'binned_contig_obj_ref': binned_contig_obj_ref
        }
        
        returnVal.update(reportVal)

        return returnVal
    
    
        
